import vndb, requests
import gzip, json, sys, os, copy
from collections import Counter
from time import sleep
from bs4 import BeautifulSoup

class pyVNCommonTagsException(Exception):
    pass

class MissingTagsGZ(pyVNCommonTagsException):
    pass

class BadPreviousResult(pyVNCommonTagsException):
    pass

class PaginationUserBreak(pyVNCommonTagsException):
    pass

class NotFoundinDB(pyVNCommonTagsException):
    pass

try:
    with gzip.GzipFile("tags.json.gz", 'r') as fin:
        tags = json.loads(fin.read().decode('utf-8'))
except FileNotFoundError:
    raise MissingTagsGZ("tags.json.gz is missing. Download tags dump at https://vndb.org/api/tags.json.gz")

vndbinstance = vndb.VNDB("pyVNCommonTags", "1", debug=False)

vnlist = [line.rstrip('\n') for line in open('vnlist.txt')]
oldvnlist = copy.copy(vnlist)
vns = []

def vn_multiple_choices(vnres, searchtype = "search"): # for readability purposes
    if not isinstance(vnres, dict):
        raise BadPreviousResult("Multiple Choice must contain the previous result")
    if searchtype == "search":
        print("There's more than one VN that contains the name '{}', specify what VN it is:".format(vnstr))
    elif searchtype == "title":
        print("There's more than one VN that is similar to '{}', specify what VN it is:".format(vnstr))
    listprinted = False
    vnrespage = 1
    while True:
        if not listprinted:
            for index, vnchoice in enumerate(vnres['items']):
                print("[{}] - https://vndb.org/v{} : {} ({})".format(index, vnchoice['id'], vnchoice['title'], vnchoice['original']))
            if vnres['more']:
                print("[10] - Next page")
            if vnrespage > 1:
                print("[11] - Previous page")
            if searchtype == "title":
                print("[12] - Break the exact search")
            listprinted = True
        try:
            vnuserpick = int(input("Pick a number from above: "))
        except ValueError:
            print("Must be a number.")
            continue

        if (vnuserpick == 10) and vnres['more']:
            print("Paginating...")
            vnrespage += 1
            vnres = vndbinstance.get('vn', 'basic,tags', '({}~"{}")'.format(searchtype, vnstr), json.dumps({ 'page' : str(vnrespage), 'sort' : 'popularity', 'reverse' : True } ) )
            listprinted = False
            continue
        elif (vnuserpick == 11):
            print("Paginating...")
            vnrespage -= 1
            vnres = vndbinstance.get('vn', 'basic,tags', '({}~"{}")'.format(searchtype, vnstr), json.dumps({ 'page' : str(vnrespage), 'sort' : 'popularity', 'reverse' : True } ) )
            listprinted = False
            continue
        elif (vnuserpick == 12):
            raise PaginationUserBreak("Exact search break by the user")

        if vnuserpick < 0:
            print("Number must be above or equal to 0.")
            continue
        elif vnuserpick > (len(vnres['items']) - 1):
            print("Number must be below or equal the last choice number ({})".format(len(vnres['items']) - 1))
            continue
        else:
            break

    return vnres, vnuserpick


for vnindex, vnstr in enumerate(vnlist):
    if vnstr.startswith("https"): # it's a vndb link, list the vn accurately
        vnres = vndbinstance.get('vn', 'basic,tags', '(id = {})'.format(''.join(filter(str.isdigit, vnstr))), json.dumps({ 'sort' : 'popularity', 'reverse' : True }))
        if len(vnres['items']) == 0: # whoa there
            raise NotFoundinDB("This entry does not exist: {}".format(vnstr))
        vns.append(vnres['items'][0])
    else: # it's probably a name... probably
        vnres = vndbinstance.get('vn', 'basic,tags', '(title~"{}")'.format(vnstr), json.dumps({ 'sort' : 'popularity', 'reverse' : True })) # specific search
        if len(vnres['items']) > 1: # there's more than 1 results, let the user pick
            try: # Try the exact search multiple choice
                vnres, vnuserpick = vn_multiple_choices(vnres = vnres, searchtype = "title")
                vns.append(vnres['items'][vnuserpick])
                vnlist[vnindex] = "https://vndb.org/v{}".format(vnres['items'][vnuserpick]['id'])
            except PaginationUserBreak: # Broke the exact search by the user, use the fuzzy search
                vnres = vndbinstance.get('vn', 'basic,tags', '(search~"{}")'.format(vnstr), json.dumps({ 'sort' : 'popularity', 'reverse' : True })) # Now trying the fuzzy search
                if len(vnres['items']) == 1: # there's only one result, same thing above (fixes the above rant)
                    vns.append(vnres['items'][0])
                    vnlist[vnindex] = "https://vndb.org/v{}".format(vnres['items'][0]['id'])
                elif len(vnres['items']) > 1: # there's more than 1 results, let the user pick
                    vnres, vnuserpick = vn_multiple_choices(vnres = vnres, searchtype = "search")
                    vns.append(vnres['items'][vnuserpick])
                    vnlist[vnindex] = "https://vndb.org/v{}".format(vnres['items'][vnuserpick]['id'])
                else: # Now it really is not exist
                    raise NotFoundinDB("Cannot find in the database: '{}'".format(vnstr))
        elif len(vnres['items']) == 1: # there's only one result
            vns.append(vnres['items'][0])
        elif len(vnres['items']) == 0:
            vnres = vndbinstance.get('vn', 'basic,tags', '(search~"{}")'.format(vnstr), json.dumps({ 'sort' : 'popularity', 'reverse' : True })) # Now trying the fuzzy search
            if len(vnres['items']) == 1: # there's only one result, same thing above (fixes the above rant)
                vns.append(vnres['items'][0])
                vnlist[vnindex] = "https://vndb.org/v{}".format(vnres['items'][0]['id'])
            elif len(vnres['items']) > 1: # there's more than 1 results, let the user pick
                vnres, vnuserpick = vn_multiple_choices(vnres = vnres, searchtype = "search")
                vns.append(vnres['items'][vnuserpick])
                vnlist[vnindex] = "https://vndb.org/v{}".format(vnres['items'][vnuserpick]['id'])
            else: # Now it really is not exist
                raise NotFoundinDB("Cannot find in the database: '{}'".format(vnstr))
        else: # for my sanity
            raise pyVNCommonTagsException("If you saw this it means that VNDB admin(s) break something in their API")
        sleep(1)

vndbinstance.close()

c = Counter()

if vnlist != oldvnlist:
    os.rename('vnlist.txt', 'vnlist-old.txt')
    with open('vnlist.txt', 'w') as f:
        for item in vnlist:
            f.write("%s\n" % item) 

for vn in vns:
    # print("Title: {} | Number of tags: {}".format(vn['title'], len(vn['tags'])))
    for tag in vn['tags']:
        c[tag[0]] += 1
    mostcommontag = list(c.most_common(10))

print("10 most common tags:")
with open('commonvntags.txt', 'w') as f:
    f.write("10 most common tags on your vnlist.txt:\n")
    for commontag in mostcommontag:
        tagsearch = list(filter(lambda tag: tag['id'] == int(commontag[0]), tags))
        print(tagsearch[0]['name'], commontag[1])
        f.write("{}\t{}\n".format(tagsearch[0]['name'], commontag[1]))