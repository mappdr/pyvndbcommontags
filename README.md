List the 10 most common tags in the list provided in vnlist.txt (accepts [partial] title or a vndb link). Uses in-disk cache and 1 second sleep to prevent hammering VNDB API. 

Requires Python 3 (will not work for Python 2). Run `pip3 install -r requirements.txt` first, then download the tags dump on `https://vndb.org/api/tags.json.gz` and put it in the same folder as this program.

Don't bother using this, just my 24-hour programming challenge. There's a better implementation for this.